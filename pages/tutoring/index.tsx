import { BlitzPage } from "@blitzjs/next"
import styles from "styles/study.module.sass"

const Tutoring: BlitzPage = () => {
	return <main className={styles.main}/>
}

export default Tutoring
